{
  config,
  lib,
  pkgs,
  ...
}: let
  inherit (builtins) elemAt;
  inherit (lib) concatStringsSep escapeShellArg genAttrs hasPrefix mapAttrsToList mkBefore mkDefault mkIf mkMerge mkOption optionalString splitString types;
  inherit (lib.types) attrsOf bool int ints lines nullOr oneOf str submodule;
  inherit (lib.types.ints) u8;

  cfg = config.turris.omnialeds;

  colorType = submodule ({...}: {
    options = genAttrs ["red" "green" "blue"] (c:
      mkOption {
        type = u8;
        description = "Intensity for the color ${c}";
      });
  });

  ledType = submodule ({
    config,
    name,
    ...
  }: let
    isPower = name == "power";
    isWan = name == "wan";
    isLan = hasPrefix "lan-" name;
    isWlan = hasPrefix "wlan-" name;
    isNet = isWan || isLan || isWlan;
    netdevId = elemAt (splitString "-" name) 1;
  in {
    options = {
      path = mkOption {
        default = "rgb:${name}";
        description = "LED path under /sys/class/leds";
        type = str;
      };

      trigger = mkOption {
        default =
          if isPower
          then "heartbeat"
          else if isNet
          then "netdev"
          else "none";
        description = "Trigger for the LED";
        type = str;
      };

      color = mkOption {
        default = let
          c = red: green: blue: {inherit red green blue;};
        in
          if isPower || isWan
          then c 0 255 0
          else if isLan
          then c 0 0 255
          else if isWlan
          then c 255 0 255
          else c 255 255 255;
        description = "Color for this LED";
        type = colorType;
      };

      settings = mkOption {
        default = {};
        description = "Options for this LED corresponding to files under this LED's directory in /sys/class/leds";
        type = attrsOf (oneOf [str int bool]);
      };

      extraStartCommands = mkOption {
        default = "";
        description = "Extra commands to set up this LED";
        type = lines;
      };

      extraStopCommands = mkOption {
        default = "";
        description = "Extra commands to shut down this LED";
        type = lines;
      };
    };

    config = {
      settings = mkMerge [
        {
          trigger = config.trigger;
          multi_intensity = "${toString config.color.red} ${toString config.color.green} ${toString config.color.blue}";
        }
        (mkIf (config.trigger == "none") {
          brightness = mkDefault 0;
        })
        (mkIf (config.trigger == "netdev" && isNet) {
          link = mkDefault 1;
          rx = mkDefault 1;
          tx = mkDefault 1;
          device_name =
            if isWan
            then "end2"
            else if isLan
            then "lan${netdevId}"
            else if isWlan
            then "wlp${netdevId}s0"
            else throw "unreachable";
        })
      ];

      extraStartCommands = mkBefore ''
        ${
          let
            makeCommand = k: v: let
              strValue =
                if v == true
                then "1"
                else if v == false
                then "0"
                else if builtins.typeOf v == "int"
                then toString v
                else v;
            in "echo ${escapeShellArg strValue} > ${escapeShellArg k}";
            makeCommand' = k: makeCommand k config.settings.${k};
          in
            concatStringsSep "\n" (
              [(makeCommand' "trigger")]
              ++ mapAttrsToList makeCommand (builtins.removeAttrs config.settings ["trigger"])
            )
        }
      '';

      extraStopCommands = mkDefault (
        if isPower
        then ''
          echo none > trigger
          echo 255 > brightness
          echo 255 255 0 > multi_intensity
        ''
        else ''
          echo none > trigger
          echo 0 > brightness
        ''
      );
    };
  });

  leds = ["power" "lan-0" "lan-1" "lan-2" "lan-3" "lan-4" "wan" "wlan-1" "wlan-2" "wlan-3" "indicator-1" "indicator-2"];
in {
  options = {
    turris.omnialeds = {
      enabled = mkOption {
        type = types.bool;
        default = true;
        description = "If Omnia LEDs setup should be enabled or not.";
      };
      brightness = mkOption {
        type = nullOr (ints.between 0 100);
        default = null;
        description = "Global brightness (overrides brightness set by the front button)";
      };

      led = genAttrs leds (led:
        mkOption {
          default = {};
          description = "Settings for LED ${led}";
          type = ledType;
        });

      extraStartCommands = mkOption {
        default = "";
        description = "Extra commands executed to setup LEDs";
        type = lines;
      };

      extraStopCommands = mkOption {
        default = "";
        description = "Extra commands executed to shut down LEDs";
        type = lines;
      };
    };
  };

  config = mkIf (config.turris.board == "omnia" && cfg.enabled) {
    # TODO modprobe triggers only if required
    boot.kernelModules = [
      "ledtrig_tty"
      "ledtrig_activity"
      "ledtrig_pattern"
      "ledtrig_netdev"
      "ledtrig_usbport"
    ];

    turris.omnialeds = {
      extraStartCommands =
        concatStringsSep "\n" (map (l: ''
            (
            cd /sys/class/leds/${escapeShellArg cfg.led.${l}.path} || exit 1
            ${cfg.led.${l}.extraStartCommands}
            ) &
          '')
          leds)
        + ''
          wait
        '';
      extraStopCommands =
        concatStringsSep "\n" (map (l: ''
            (
            cd /sys/class/leds/${escapeShellArg cfg.led.${l}.path} || exit 1
            ${cfg.led.${l}.extraStopCommands}
            ) &
          '')
          leds)
        + ''
          wait
        '';
    };

    systemd.services."omnia-leds" = {
      wantedBy = ["multi-user.target"];
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
        ExecStart = pkgs.writeShellScript "start-leds" ''
          ${optionalString (cfg.brightness != null)
            "echo ${toString cfg.brightness} > /sys/devices/platform/soc/soc:internal-regs/f1011000.i2c/i2c-0/i2c-1/1-002b/brightness"}
          ${cfg.extraStartCommands}
        '';
        ExecStop = pkgs.writeShellScript "stop-leds" ''
          ${cfg.extraStopCommands}
        '';
      };
    };
  };
}
